import { MoviesContainer, MoviesTitle, MoviesRow, MoviesPoster} from "./Movies.styles";
import Swal from 'sweetalert2/src/sweetalert2.js';

import './Movies.css';

function Movies({ title, movies }) {
    return (
        <MoviesContainer>
            <MoviesTitle>{title}</MoviesTitle>
            <MoviesRow>
                {movies.map((movie) => (
                    <MoviesPoster
                        key={movie.id}
                        src={"https://image.tmdb.org/t/p/w300" + movie.poster_path}
                        alt={movie.name}
                        onClick={() => Swal.fire({
                            title: `${movie.name}`,
                            text: `${movie.overview}`,
                            imageUrl: `${"https://image.tmdb.org/t/p/w300" + movie.poster_path}`,
                            imageHeight: 300,
                            imageAlt: `${movie.name}`,
                        })}
                    />
                ))}
            </MoviesRow>
        </MoviesContainer>
    );
}

export default Movies;