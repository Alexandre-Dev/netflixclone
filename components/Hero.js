import React from "react";
import {HeroButton, HeroContainer, HeroDescription, HeroTitle} from "./Hero.styles";
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

const Sw2 = withReactContent(Swal)

function Hero({ movie }) {
    console.log(movie);
    return (
        <HeroContainer background={movie?.backdrop_path}>
            <HeroTitle>{movie?.name}</HeroTitle>
            <HeroDescription>{movie?.overview}</HeroDescription>
            <HeroButton>Lire</HeroButton>
            <HeroButton onClick={() => Sw2.fire({
                title: `Voulez-vous ajoutez ${movie?.name} a votre liste ?`,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui, ajoutez le!',
                cancelButtonText: 'Non merci',
            }).then((result) => {
                if (result.isConfirmed) {
                    Sw2.fire(
                        'Parfait !',
                        `${movie?.name} a été ajoutez avec succès`,
                        'success'
                    )
                }
            })}>Ajoutez a ma liste</HeroButton>
        </HeroContainer>
    );
}

export default Hero;