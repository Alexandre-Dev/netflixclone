import styled from 'styled-components';

export const MoviesContainer = styled.div`
  margin-top: 2rem;
  margin-bottom: 2rem;
`;

export const MoviesTitle = styled.h2`
  font-size: 1.5rem;
  line-height: 2rem;
  font-weight: 700;
  text-transform: uppercase;
  margin-right: 2rem;
  margin-left: 2rem;
`;

export const MoviesRow = styled.div`
  display: flex;
  overflow-x: auto;
  margin-top: 1rem;
  padding: 1rem;
  
  -ms-overflow-style: none;
  scrollbar-width: none;

  &::-webkit-scrollbar {
    display: none;
  }
`;

export const MoviesPoster = styled.img`
  margin: 0.5rem;
  width: 10rem;
  

  transition: all 0.2s;
  &:hover {
    transform: scale(1.09);
  }
`;

