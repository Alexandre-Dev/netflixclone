import styled from "styled-components";

export const HeroContainer = styled.div`
  padding: 5rem;
  height: 70vh;
  background-size: cover !important;
  background-position: center !important;

  ${(props) =>
    `background: url('https://image.tmdb.org/t/p/original${props.background}');`}
`;

export const HeroTitle = styled.h1`
  font-size: 3rem;
  line-height: 1rem;
  font-weight: 700;
  margin-bottom: 2rem;
  margin-top: 25vh;
  color: white;
`;

export const HeroDescription = styled.p`
  font-weight: 500;
  font-size: 1.125rem;
  line-height: 1.75rem;
  font-weight: 
  margin-bottom: 1rem;
  max-width: 45rem;
  line-height: 1.3;
  color:white;
`;

export const HeroButton = styled.button`
  cursor: pointer;
  color: #fff;
  outline: none;
  border: none;
  font-weight: 700;
  border-radius: .2vw;
  margin-right: 1rem;
  background-color: rgba(51,51,51,.5);
  padding: .5rem 2rem;
  

  &:hover {
    background-color: #e6e6e6;
    color: black !important;
    transition: all 0.2s;
  }
`;
