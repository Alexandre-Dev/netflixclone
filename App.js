import { useState, useEffect } from 'react';
import axios from 'axios';
import './App.css';

import Movies from "./components/Movies";
import Hero from "./components/Hero";

const URL = "https://api.themoviedb.org/3";
const API_KEY = "81fa7a8117aa92fd681825f656cffcc7";

const endpoints = {
  trending: "/movie/popular",
  popular: "/tv/popular",
  top_rated: "/tv/top_rated",
};

function App() {

  const [trending, setTrending] = useState([]);
  const [popular, setPopular] = useState([]);
  const [popularSF, setPopularSF] = useState([]);
  const [topRated, setTopRated] = useState([]);


  useEffect(() => {
    // Load Trending
    axios
        .get(`${URL}${endpoints.trending}`, {
          params: {
            api_key: API_KEY,
            language: 'fr',
          },
        })
        .then((res) => setTrending(res.data.results));

    // Load Popular
    axios
        .get(`${URL}${endpoints.popular}`, {
          params: {
            api_key: API_KEY,
            language: 'fr',
          },
        })
        .then((res) => setPopular(res.data.results));

      // Load Popular SF
      axios
          .get(`${URL}${endpoints.popular}`, {
              params: {
                  api_key: API_KEY,
                  language: 'fr',
                  with_genres: '10765',
              },
          })
          .then((res) => setPopularSF(res.data.results));
    // Load Top Rated
    axios
        .get(`${URL}${endpoints.top_rated}`, {
          params: {
            api_key: API_KEY,
            language: 'fr',
          },
        })
        .then((res) => setTopRated(res.data.results));
  }, []);

  return (
    <>
        <Hero movie={popularSF[Math.floor(Math.random() * popularSF.length)]} />
        <Movies title="POPULAIRE (FILMS)" movies={trending} />
        <Movies title="POPULAIRE (SERIES)" movies={popular} />
        <Movies title="Series SF et Fantastique" movies={popularSF} />
        <Movies title="LES MIEUX NOTÉ" movies={topRated} />
    </>
  );
}

export default App;
