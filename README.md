# Movie Explorer App

## Overview

Movie Explorer is a web application designed to allow users to browse and discover trending, popular, and top-rated movies and TV shows. It features a user-friendly and responsive interface that showcases cinematic works with images, titles, and descriptions. Users can also add movies or TV shows to their personal list and get additional details by clicking on the posters.

## Features

- **Film and TV Show Discovery**: Users can view trending, popular, and top-rated movies and TV series.
- **Random Hero Feature**: A movie or TV show is randomly featured in the hero section with a backdrop image, title, and overview.
- **Interactive Lists**: Films and series are organized in lists that users can interact with by clicking on posters to read more or add to their list.
- **Alerts and Confirmations**: Integration with SweetAlert2 for interactive alerts and confirmations when adding items to the user's list.

## Technologies Used

- **React**: A JavaScript library for building user interfaces.
- **Axios**: A promise-based HTTP client for making API requests.
- **Styled Components**: A library for styling React components using tagged template literals.
- **SweetAlert2**: A beautiful, responsive, customizable, and accessible replacement for JavaScript's popup boxes.

## How to Use

To use the application, simply visit the deployed web page. You can browse through the different categories of movies and TV shows. Click on a poster to view more details about a particular film or series. If you wish to add a movie or show to your list, click the 'Add to My List' button and confirm your choice in the popup alert.

## Storage

The application does not currently implement persistent storage for the user's list. The list is session-based and will reset upon refreshing or closing the browser.

## Running the Application

To run the application locally, you will need Node.js and npm installed on your system. Follow these steps:

1. Clone the repository to your local machine.
2. Navigate to the project directory in your terminal.
3. Install the dependencies with `npm install`.
4. Start the development server with `npm start`.
5. Open your browser and go to `http://localhost:3000` to view the app.

Note: The application requires an API key from The Movie Database (TMDb). Ensure you have your own API key and that it is correctly set up in the application's environment variables.
